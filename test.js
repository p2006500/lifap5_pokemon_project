const donnees_exemple = [
    {
        "Abilities": [
          "Torrent",
          "Rain Dish"
        ],
        "Against": {
          "Bug": 1,
          "Dark": 1,
          "Dragon": 1,
          "Electric": 2,
          "Fairy": 1,
          "Fight": 1,
          "Fire": 0.5,
          "Flying": 1,
          "Ghost": 1,
          "Grass": 2,
          "Ground": 1,
          "Ice": 0.5,
          "Normal": 1,
          "Poison": 1,
          "Psychic": 1,
          "Rock": 1,
          "Steel": 0.5,
          "Water": 0.5
        },
        "Attack": 103,
        "BaseEggSteps": 5120,
        "BaseHappiness": 70,
        "BaseTotal": 630,
        "CaptureRate": 45,
        "Classification": "",
        "Defense": 120,
        "ExperienceGrowth": 1059860,
        "Generation": 1,
        "HeightM": 1.6,
        "Hp": 79,
        "Images": {
          "Full": "https://assets.pokemon.com/assets/cms2/img/pokedex/full/009.png",
          "Detail": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/009.png"
        },
        "IsLegendary": 0,
        "JapaneseName": "Kamexカメックス",
        "Name": "Blastoise",
        "PercentageMale": 88.1,
        "PokedexNumber": 9,
        "SpAttack": 135,
        "SpDefense": 115,
        "Speed": 78,
        "Types": [
          "water"
        ],
        "WeightKg": 85.5
      },
      {
        "Abilities": [
          "Swarm",
          "Sniper"
        ],
        "Against": {
          "Bug": 0.5,
          "Dark": 1,
          "Dragon": 1,
          "Electric": 1,
          "Fairy": 0.5,
          "Fight": 0.25,
          "Fire": 2,
          "Flying": 2,
          "Ghost": 1,
          "Grass": 0.25,
          "Ground": 1,
          "Ice": 1,
          "Normal": 1,
          "Poison": 0.5,
          "Psychic": 2,
          "Rock": 2,
          "Steel": 1,
          "Water": 1
        },
        "Attack": 150,
        "BaseEggSteps": 3840,
        "BaseHappiness": 70,
        "BaseTotal": 495,
        "CaptureRate": 45,
        "Classification": "",
        "Defense": 40,
        "ExperienceGrowth": 1000000,
        "Generation": 1,
        "HeightM": 1,
        "Hp": 65,
        "Images": {
          "Full": "https://assets.pokemon.com/assets/cms2/img/pokedex/full/015.png",
          "Detail": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/015.png"
        },
        "IsLegendary": 0,
        "JapaneseName": "Spearスピアー",
        "Name": "Beedrill",
        "PercentageMale": 50,
        "PokedexNumber": 15,
        "SpAttack": 15,
        "SpDefense": 80,
        "Speed": 145,
        "Types": [
          "bug",
          "poison"
        ],
        "WeightKg": 29.5
      },
      {
        "Abilities": [
          "Compoundeyes",
          "Tinted Lens"
        ],
        "Against": {
          "Bug": 0.5,
          "Dark": 1,
          "Dragon": 1,
          "Electric": 2,
          "Fairy": 1,
          "Fight": 0.25,
          "Fire": 2,
          "Flying": 2,
          "Ghost": 1,
          "Grass": 0.25,
          "Ground": 0,
          "Ice": 2,
          "Normal": 1,
          "Poison": 1,
          "Psychic": 1,
          "Rock": 4,
          "Steel": 1,
          "Water": 1
        },
        "Attack": 45,
        "BaseEggSteps": 3840,
        "BaseHappiness": 70,
        "BaseTotal": 395,
        "CaptureRate": 45,
        "Classification": "",
        "Defense": 50,
        "ExperienceGrowth": 1000000,
        "Generation": 1,
        "HeightM": 1.1,
        "Hp": 60,
        "Images": {
          "Full": "https://assets.pokemon.com/assets/cms2/img/pokedex/full/012.png",
          "Detail": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/012.png"
        },
        "IsLegendary": 0,
        "JapaneseName": "Butterfreeバタフリー",
        "Name": "Butterfree",
        "PercentageMale": 50,
        "PokedexNumber": 12,
        "SpAttack": 90,
        "SpDefense": 80,
        "Speed": 70,
        "Types": [
          "bug",
          "flying"
        ],
        "WeightKg": 32
      }
]

const etatCourant={pokemons:donnees_exemple,search: undefined};

suite('Tests pour la fonction triPokemonParID', function() {
    test("On vérifie l'ordre des ID sur tri sur les données exemple", function() {
      const resultat_attendu = [
        9,
        12,
        15
      ];
      const tabID = triPokemonParId(1,donnees_exemple)
      .map((n) => n.PokedexNumber);
      chai.assert.deepEqual(tabID, resultat_attendu);
    });
    
});

suite('Tests pour la fonction triPokemonParNom', function() {
  test("On vérifie l'ordre des ID sur tri sur les données exemple", function() {
    const resultat_attendu = [
      15,
      9,
      12
    ];
    const tabID = triPokemonParNom(0,donnees_exemple)
    .map((n) => n.PokedexNumber);
    chai.assert.deepEqual(tabID, resultat_attendu);
  });
  
});

suite('Tests pour la fonction triPokemonParAbility', function() {
  test("On vérifie l'ordre des ID sur tri sur les données exemple", function() {
    const resultat_attendu = [
      12,
      15,
      9
    ];
    const tabID = triPokemonParAbility(0,donnees_exemple)
    .map((n) => n.PokedexNumber);
    chai.assert.deepEqual(tabID, resultat_attendu);
  });
  
});

suite('Tests pour la fonction triPokemonParType', function() {
  test("On vérifie l'ordre des ID sur tri sur les données exemple", function() {
    const resultat_attendu = [
      12,
      15,
      9
    ];
    const tabID = triPokemonParType(0,donnees_exemple)
    .map((n) => n.PokedexNumber);
    chai.assert.deepEqual(tabID, resultat_attendu);
  });
  
});

suite('Tests pour la fonction fetchPokemon', function() {
  test("On vérifie l'ordre des ID sur tri sur les données exemple", function() {
    const resultat_attendu = [
      9,
      15,
      12
    ];
    const tabID = fetchPokemon().then(json => 
      json.map((n) => n.PokedexNumber).slice(0,3)
    );
    //On obtient toutes les données du serveur, mais on teste sur les 3 premières
    tabID.then((tab) => chai.assert.deepEqual(tab, resultat_attendu));
  });
  
});