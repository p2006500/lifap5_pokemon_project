/* ******************************************************************
* Constantes de configuration
* ****************************************************************** */
const apiKey = "50b40569-c4cd-48ac-81bf-7c7b44a874d6"; //"69617e9b-19db-4bf7-a33f-18d4e90ccab7";
const serverUrl = "https://lifap5.univ-lyon1.fr";

/* ******************************************************************
* Gestion de la boîte de dialogue (a.k.a. modal) d'affichage de
* l'utilisateur.
* ****************************************************************** */

/**
 * Fait une requête GET authentifiée sur /whoami avec la clef API fournit
 * @param API clef API
 * @returns une promesse du login utilisateur ou du message d'erreur
 */
function fetchWhoami(API) {
  return fetch(serverUrl + "/whoami", { headers: { "Api-Key":  API} })
    .then((response) => {
      if (response.status === 401) {
        return response.json().then((json) => {
        console.log(json);
        return { err: json.message };
      });
    } else {
      return response.json();
    }
    })
    .catch((erreur) => ({ err: erreur }));
}

function fetchPokemon() {
  return fetch(serverUrl + "/pokemon", { headers: { "Api-Key": apiKey } })
    .then((response) => {
      if (response.status !== 200) {
        return response.json().then((json) => {
          console.log(json);
          return { err: json.message };
        });
      } else {
        return response.json();
      }
    })
    .catch((erreur) => ({ err: erreur }));
}

/**
 * Fait une requête sur le serveur et insère le login dans la modale d'affichage
 * de l'utilisateur puis cache cette modale.
 * @param {Etat} etatCourant l'état courant
 * @param apikey clef API
 * @returns Une promesse de mise à jour
 */
function lanceWhoamiEtInsereLogin(apikey,etatCourant) {
  return fetchWhoami(apikey).then((data) => {
    majEtatEtPage(etatCourant, {
      login: data.user, // qui vaut undefined en cas d'erreur
      errLogin: data.err, // qui vaut undefined si tout va bien
      loginModal: false, // on cache la modale
    });
  });
}

/**
 * Genère le code HTML de la modale de connexion pour insérer la clef api
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML ainsi qu'un objet lançant un fetch
 *  de la clef fournit si l'on click sur le bouton valider
 */
function genereModalToConnect(etatCourant){
  return{
    html:`
    <section class="modal-card-body">
      <p>
        <input type="text" id="APIKEY" 
        placeholder="veuillez inscrire votre id">
      </p>
    </section>  
    `,
    callbacks:{
      "btn-submit-login-modal2": {
        onclick: ()=>lanceWhoamiEtInsereLogin(
          document.getElementById("APIKEY").value,etatCourant)
      }
    }
  }
}


/**
 * Génère le code HTML du corps de la modale de login. On renvoie en plus un
 * objet callbacks vide pour faire comme les autres fonctions de génération,
 * mais ce n'est pas obligatoire ici.
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et un objet vide
 * dans le champ callbacks
 */
function genereModaleLoginBody(etatCourant) {
  const text =
    etatCourant.errLogin !== undefined
      ? etatCourant.errLogin
      : etatCourant.login;
  return {
    html: `
  <section class="modal-card-body">
    <p>
      ${text}
    </p>
  </section>
  `,
    callbacks: {},
  };
}

/**
 * Génère le code HTML du titre de la modale de login et les callbacks associés.
 *
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereModaleLoginHeader(etatCourant) {
  return {
    html: `
<header class="modal-card-head  is-back">
  <p class="modal-card-title">Utilisateur</p>
  <button
    id="btn-close-login-modal1"
    class="delete"
    aria-label="close"
    ></button>
</header>`,
    callbacks: {
      "btn-close-login-modal1": {
        onclick: () => majEtatEtPage(etatCourant, { loginModal: false }),
      },
    },
  };
}

/**
 * Génère le code HTML du base de page de la modale de login et les callbacks associés.
 *
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereModaleLoginFooter(etatCourant) {
  return {
    html: `
  <footer class="modal-card-foot" style="justify-content: flex-end">
    <button id="btn-close-login-modal2" class="button">Fermer</button>
    <button id="btn-submit-login-modal2" 
    class="is-sucess button">valider</button>
  </footer>
  `,
    callbacks: {
      "btn-submit-login-modal2":{
        onclick: () => majEtatEtPage(etatCourant, {loginModal:false})
      },
      "btn-close-login-modal2": {
        onclick: () => majEtatEtPage(etatCourant, { loginModal: false }),
      },
    },
  };
}

/**
 * Génère le code HTML de la modale de login et les callbacks associés.
 *
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereModaleLogin(etatCourant) {
  const header = genereModaleLoginHeader(etatCourant);
  const footer = genereModaleLoginFooter(etatCourant);
  const body = genereModalToConnect(etatCourant);
  const activeClass = etatCourant.loginModal ? "is-active" : "is-inactive";
  return {
    html: `
      <div id="mdl-login" class="modal ${activeClass}">
        <div class="modal-background"></div>
        <div class="modal-card">
          ${header.html}
          ${body.html}
          ${footer.html}
        </div>
      </div>`,
    callbacks: { ...header.callbacks, ...footer.callbacks, ...body.callbacks },
  }
}

/* ************************************************************************
 * Gestion de barre de navigation contenant en particulier les bouton Pokedex,
 * Combat et Connexion.
 * ****************************************************************** */

/**
 * Déclenche la mise à jour de la page en changeant l'état courant pour que la
 * modale de login soit affichée
 * @param {Etat} etatCourant
 */
function afficheModaleConnexion(etatCourant) {
  majEtatEtPage(etatCourant, {loginModal:true})
}


/**
 * Génère le code HTML et les callbacks pour la partie droite de la barre de
 * navigation qui contient le bouton de login.
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereBoutonConnexion(etatCourant) {
  const text=
  etatCourant.login!==undefined ? "déconnexion":"connexion";
  const html = `
  <div class="navbar-end">
    <div class="navbar-item">
      <div class="buttons">
        <a id="btn-open-login-modal" class="button is-light"> ${text} </a>
      </div>
    </div>
  </div>`;
  return {
    html: html, callbacks: {
      "btn-open-login-modal": {
        onclick : etatCourant.login!==undefined ? ()=>
        majEtatEtPage(etatCourant,{login:undefined}):
        ()=>afficheModaleConnexion(etatCourant)
      }
    }
  };
}

/**
 * Fonction qui génére un objet html si l'utilisateur est connecté
 * @param {Etat} etatCourant
 * @returns un objet contenant un objet html et un callbacks
 */
function showLogin(etatCourant){
  const html=`
  <div class="navbar-end">
    <div class="navbar-item">
      <p id="elt-affichage-login">
        ${etatCourant.login}
      <p>
    </div>
  </div>`;
  return{
    html:html,
    callbacks:{}
  };
}


/**Déclenche une requête sur le serveur et déclenche 
 *la MAJ de la page avec la liste des pokemons
 *La fonction initialise aussi les champs d'etatCourant à initialiser
 *@param {Etat} etatCourant
 *@returns une promesse 
 */
function recuperePokemon(etatCourant) {
  return fetchPokemon() //Promesse de fetch
  .then(json => majEtatEtPage(etatCourant,{
    nbPok:10,
    type_tri:1, //Le type '1' sera le tri par ID
    ordre_tri:1, //Si le tri est pair, l'ordre sera ascendant, descendant sinon
    search:'',
    pokemons:json,
    selectedPok:json[18],
  }));
  //On met à jour le champ "pokemons"
  //On met Bulbisar en pokemon selectionné par défaut 
}

/**Fonction qui créer un callback onclick pour changer le pokemon sélectionné sur un id 
 * @param {Etat} etatCourant
 * @param poke
 * @returns un callback pour changer le pokemon selectionné
*/
function callbackOnclickSelectedPok(etatCourant, poke)
{
  const callback = {};
  callback[`tabLigne${poke.PokedexNumber}`] 
  = {"onclick": () => majEtatEtPage(etatCourant,{selectedPok:poke}) };
  return callback;
}

/**Fonction qui créer les 4 callbacks onclick pour changer l'ordre de tri du tableau
 * @param {Etat} etatCourant
 * @returns un tableau de callback pour changer le type de tri
*/
function callbacksOnclickTri(etatCourant)
{
  if(etatCourant.type_tri === undefined)
  {
    return {html:"",
  callbacks:{}}
  }
  else
  {
    return {html:"",
    callbacks:  {
      "tri_id": {"onclick": () => majEtatEtPage(etatCourant
      ,{type_tri:1,ordre_tri:etatCourant.ordre_tri + 1}) },
      "tri_name": {"onclick": () => majEtatEtPage(etatCourant
      ,{type_tri:2,ordre_tri:etatCourant.ordre_tri + 1}) },
      "tri_abilities": {"onclick": () => majEtatEtPage(etatCourant
      ,{type_tri:3,ordre_tri:etatCourant.ordre_tri + 1}) },
      "tri_types": {"onclick": () => majEtatEtPage(etatCourant
      ,{type_tri:4,ordre_tri:etatCourant.ordre_tri + 1}) }
    }}
  }
}

/** Fonction qui trie le tableau par ID (numéro du pokedex)
 * @param sens_tri ascendant si impair, descendant sinon
 * @param tabPokemon
 * @returns un tableau de pokemons triés en fonction de l'ID
*/
function triPokemonParId(sens_tri, tabPokemon)
{
  return (sens_tri % 2 === 1 ? tabPokemon.sort( 
        (a,b) => a.PokedexNumber-b.PokedexNumber)
        : tabPokemon.sort( 
          (a,b) => b.PokedexNumber-a.PokedexNumber));
}

/** Fonction qui trie le tableau par Nom
 * @param sens_tri ascendant si pair, descendant sinon
 * @param tabPokemon
 * @returns un tableau de pokemons triés en fonction de l'ID
*/
function triPokemonParNom(sens_tri, tabPokemon)
{
  return (sens_tri % 2 === 1 ? tabPokemon.sort( 
        (a,b) => a.Name < b.Name)
        : tabPokemon.sort( 
          (a,b) => a.Name > b.Name));
}

/** Fonction qui trie le tableau par Ability
 * @param sens_tri ascendant si pair, descendant sinon
 * @param tabPokemon
 * @returns un tableau de pokemons triés en fonction de l'ID
*/
function triPokemonParAbility(sens_tri, tabPokemon)
{
  return (sens_tri % 2 === 1 ? tabPokemon.sort( 
        (a,b) => a.Abilities[0] < b.Abilities[0])
        : tabPokemon.sort( 
          (a,b) => a.Abilities[0] > b.Abilities[0]));
}

/** Fonction qui trie le tableau par type
 * @param sens_tri ascendant si pair, descendant sinon
 * @param tabPokemon
 * @returns un tableau de pokemons triés en fonction de l'ID
*/
function triPokemonParType(sens_tri, tabPokemon)
{
  return (sens_tri % 2 === 1 ? tabPokemon.sort( 
        (a,b) => a.Types[0] < b.Types[0])
        : tabPokemon.sort( 
          (a,b) => a.Types[0] > b.Types[0]));
}


/** Fonction qui trie le tableau de pokemon en fonction de tri voulu
 * 
 * @param type_tri 
 * @param sens_tri ascendant si pair, descendant sinon
 * @param tabPokemon
 * @returns un tableau de pokemons triés
 */
function triPokemon(type_tri, sens_tri, tabPokemon)
{
    if(type_tri === 1) //On tri par ID
    {
      return triPokemonParId(sens_tri, tabPokemon);
    }
    else if(type_tri === 2) //on tri par nom
    {
      return triPokemonParNom(sens_tri, tabPokemon);
    }
    else if(type_tri === 3) //on tri par ability
    {
      return triPokemonParAbility(sens_tri, tabPokemon);
    }
    else if(type_tri === 4) //on tri par type
    {
      return triPokemonParType(sens_tri, tabPokemon);
    }
}

/** Fonction qui change la classe de la ligne sélectionnée pour qu'elle soit verte
 * @param ligneTab une ligne du tableau
 * @param {Etat} etatCourant
 * @returns "is-selected" si la ligne est sélectionnée, rien sinon
 */
function genereClassSelected(pokemon, etatCourant)
{
  return (pokemon === etatCourant.selectedPok) ? "is-selected" : ''; 

}

  /**Fonction qui génère l'html d'une ligne du tableau de pokemon
   * @param n pokemon
   * @param {Etat} etatCourant
   * @returns du code html
   */
  function genereTabLignePokemon(n,etatCourant) 
  {
    return ` <tr id="tabLigne${n.PokedexNumber}"
    class="${genereClassSelected(n,etatCourant)}"> 
    <td>
    <img
    alt="${n.Name}"
    src="${n.Images.Detail}"
    width="64"
    />
    </td>
    <td><div class="content">${n.PokedexNumber}</div></td>
    <td><div class="content">${n.Name}</div></td>
    <td>
    <ul>
    ${n.Abilities.map( (a) => `<li> ${a} </li>`).join('\n')}
    </ul>
    </td>
    <td>
    <ul>
    ${n.Types.map( (a) => `<li> ${a} </li>`).join('\n')}
    </ul>
    </td> 
    </tr>`;
  }
  
  /** Fonction qui génère le header du tableau de pokemons
   * @returns du code html
   */
  function genenreHeaderTabPokemon()
  {
    return `<thead>
    <tr>
    <th><span>Image</span></th>
    <th>
    <span id="tri_id">#</span
    ><span class="icon"><i class="fas fa-angle-up"></i></span>
    </th>
    <th><span id="tri_name">Name</span></th>
    <th><span id="tri_abilities">Abilities</span></th>
    <th><span id="tri_types">Types</span></th>
    </tr>
    </thead>`;
  }
  
  /** Fonction qui ouvre et ferme le tableau de pokemon
   * @param tab tableau de pokemon
   * @returns du code html
   */
  function OuvreFermeTabPokemon(tab)
  {
    return `
    <div class="column">
    <div id="tbl-pokemons">
    <table class="table">
    ${genenreHeaderTabPokemon()}
    <tbody>`
    + tab + 
    `</tbody>
    </table>
    </div>
    </div>`;
  }
  
  /**Fonction qui génère l'html de la liste des pokemon avec l'état courant de la page
   * @param {Etat} etatCourant
   * @returns un objet contenant le code HTML dans le champ html
   */
  function genereTabPokemon(etatCourant) {
    //On commence par changer le contenu de chaque case (tr) du tableau
    //On rajoute que sur chaque ligne, lorsque qu'on clique dessus, celà
    //met à jour le champ "selectedPokemon"
    //On utilise la fonction "slice" pour n'afficher qu'une partie de la liste des pokemons
    if(etatCourant.pokemons !== undefined)
    {
      const filterTabPokemon=filterPokemon(etatCourant);
      const html = triPokemon(etatCourant.type_tri, 
        etatCourant.ordre_tri, etatCourant.search==='' ?
        etatCourant.pokemons:filterTabPokemon)
      .slice(0,etatCourant.nbPok)
      .map( (n) => genereTabLignePokemon(n,etatCourant)).join('');   //Transforme le tableau en chaîne de caractères 
      //On rajoute les entêtes au début, et les fermetures de balise à la fin
      //On va créer un callback pour chaque ligne du tableau, pour afficher les statistiques de chaque pokemon
      //quand on clique dessus
      const ligneGenerees = etatCourant.search==='' ?   //Tableau de callbacks
      etatCourant.pokemons
      .slice(0,etatCourant.nbPok)
      .map( (n) => callbackOnclickSelectedPok(etatCourant,n)):
      filterTabPokemon
      .slice(0,etatCourant.nbPok)
      .map((n)=>callbackOnclickSelectedPok(etatCourant,n)); //Tableau de callbacks
      //On va créer un callback pour chaque type de header, pour modifier ce par quoi on tri le tableau 
    return {html:OuvreFermeTabPokemon(html),
      callbacks:ligneGenerees.reduce( (acc,l) => ( {...acc, ...l }), {}),
    };
  }
  else 
  return {html:"Pas de pokemon",callbacks:{}};
}

/**Fonction qui génère l'html du titre d'une case du tableau de pokemon
 * @param pokemon
 * @returns du code html
 */
function genereTitreCaseChoisi(pokemon)
{
  return `
  <div class="card-header">
    <div class="card-header-title">${pokemon.JapaneseName} 
    (#${pokemon.PokedexNumber})</div>
  </div>
  <div class="card-content">
    <article class="media">
      <div class="media-content">
        <h1 class="title">${pokemon.Name}</h1>
      </div>
    </article>
  </div>`
}

/**Fonction qui génère le code html du contenu de la case du pokemon choisi
 * @param pokemon
 * @param tab_res des types auxquels le pokemon résiste
 * @param tab_str des types auxquels le pokemon inflige plus de dégats
 * @returns du code html
 */
 function genereContenuCasePokemon(pokemon,tab_res,tab_str)
 {
   return `
   <div class="media-content">
      <div class="content has-text-left">
        <p>Hit points: ${pokemon.Hp}</p>
        <h3>Abilities</h3>
          <ul>
            ${pokemon.Abilities.map( (a) => `<li> ${a} </li>`).join('\n')}
          </ul>
          <h3>Resistant against</h3>
          <ul>
            ${tab_res.map( (r) => `<li> ${r} </li>`).join('')}
          </ul>
          <h3>Weak against</h3>
          <ul>
            ${tab_str.map( (r) => `<li> ${r} </li>`).join('')}
          </ul>
      </div>
    </div>`
 }

 /**Fonction qui génère le code html pour afficher l'image d'un pokemon
 * @param pokemon
 * @returns du code html
 */
  function genereImagePokemon(pokemon)
  {
    return `
    <figure class="media-right">
      <figure class="image is-475x475">
        <img
          class=""
          src="${pokemon.Images.Detail}"
          alt="${pokemon.Name}"
        />
      </figure>
    </figure>`
  }

  /**Fonction qui génère le code html pour afficher le pokemon sélectionné 
   * @param pokemon
   * @returns du code html
   */
  function genereSelectedPokemonHtml(pokemon)
  {
    const against_tab = Object.keys(pokemon.Against); //On transforme l'objet against en tableau
    const against_tab_res = against_tab.filter( (n) => 
    pokemon.Against[n] < 1); //On récupère un tableau avec les types efficaces
    const against_tab_str = against_tab.filter( (n) => 
    pokemon.Against[n] > 1);
    return `
    <div class="column">
      <div class="card">
        ${genereTitreCaseChoisi(pokemon)}
        <div class="card-content">
          <article class="media">
            ${genereContenuCasePokemon(pokemon
              ,against_tab_res,against_tab_str)}
            ${genereImagePokemon(pokemon)}
          </article>
        </div>
      </div>
    </div>`
  }


/**Fonction qui génère l'html du pokemon courant si selectedPok est défini
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html
 */
function genereSelectedPokemon(etatCourant) {
 
  if(etatCourant.selectedPok !== undefined)
  {
  return {html:genereSelectedPokemonHtml(etatCourant.selectedPok),
      callbacks: {}};
  }
    else
    {
      return {html:"Pas de pokemon",callbacks:{}};
    }
}

/**Fonction qui génère le formulaire pour ajouter et enlever des pokemons
 * @returns un formulaire html
 */
function genereFormMoreLess()
{
  return `<form>
            <button class="button is-light" id="morePok"> Plus </button>
            <button class="button is-light" id="lessPok"> Moins </button>
          </form>`;
}

/**
 * Génère le code HTML du bouton permettant de changer le nombre de pokemon afficher
 * @param {*} etatCourant 
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereBoutonLimitationPokemon(etatCourant) {
  if(etatCourant.nbPok !== undefined)
  {
    const callbacks =
    {
      "morePok": {
        onclick: () => { majEtatEtPage(etatCourant
          ,{nbPok:etatCourant.nbPok + 10}) }
      },
      "lessPok":{
        onclick: () => { if(etatCourant.nbPok > 10) 
          { majEtatEtPage(etatCourant,{nbPok:etatCourant.nbPok - 10}) } }
      }
    }
    return {html:genereFormMoreLess(), callbacks:callbacks};
  }
  else 
  {
    return {html:"",callbacks:{}};
  }
}

  /**
   * Génère le code HTML de la barre de navigation et les callbacks associés.
   * @param {Etat} etatCourant
   * @returns un objet contenant le code HTML dans le champ html et la description
   * des callbacks à enregistrer dans le champ callbacks
   */
  function genereBarreNavigation(etatCourant) {
    const connexion = genereBoutonConnexion(etatCourant);
    const login= etatCourant.login!==undefined ? 
    showLogin(etatCourant).html:'';
    const search=genereSearchPokemon(etatCourant);
    const button_refresh=genereRefreshPokemon(etatCourant); 
    return {
      html: `
      <nav class="navbar" role="navigation" aria-label="main navigation">
      <div class="navbar">
      <div class="navbar-item"><div class="buttons">
      <a id="btn-pokedex" class="button is-light"> Pokedex </a>
      <a id="btn-combat" class="button is-light"> Combat </a>
      </div></div>
      ${connexion.html} ${login} ${search.html} ${button_refresh.html} 
      </div>
      </nav>`,
    callbacks: {
      ...connexion.callbacks, ...search.callbacks, ...button_refresh.callbacks,
      "btn-pokedex": { onclick: () => console.log("click bouton pokedex") },
    },
  };
}

/**
 * Génére le code HTML de la page ainsi que l'ensemble des callbacks à
 * enregistrer sur les éléments de cette page.
 *
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function generePage(etatCourant) {
  const barredeNavigation = genereBarreNavigation(etatCourant);
  const modaleLogin = genereModaleLogin(etatCourant);
  const ListePokemon = genereTabPokemon(etatCourant);
  const SelectedPokemon = genereSelectedPokemon(etatCourant);
  const LimitePokemon = genereBoutonLimitationPokemon(etatCourant);
  const tri=callbacksOnclickTri(etatCourant);
  // remarquer l'usage de la notation ... ci-dessous qui permet de "fusionner"
  // les dictionnaires de callbacks qui viennent de la barre et de la modale.
  // Attention, les callbacks définis dans modaleLogin.callbacks vont écraser
  // ceux définis sur les mêmes éléments dans barredeNavigation.callbacks. En
  // pratique ce cas ne doit pas se produire car barreDeNavigation et
  // modaleLogin portent sur des zone différentes de la page et n'ont pas
  // d'éléments en commun.
  return {
    html: barredeNavigation.html + modaleLogin.html
    + SelectedPokemon.html 
    + ListePokemon.html + LimitePokemon.html,
    callbacks: { ...barredeNavigation.callbacks, ...modaleLogin.callbacks, 
      ...ListePokemon.callbacks,
      ...LimitePokemon.callbacks,
      ...tri.callbacks}
  };
}

/* ******************************************************************
 * Initialisation de la page et fonction de mise à jour
 * globale de la page.
 * ****************************************************************** */

/**
 * Créée un nouvel état basé sur les champs de l'ancien état, mais en prenant en
 * compte les nouvelles valeurs indiquées dans champsMisAJour, puis déclenche la
 * mise à jour de la page et des événements avec le nouvel état.
 *
 * @param {Etat} etatCourant etat avant la mise à jour
 * @param {*} champsMisAJour objet contenant les champs à mettre à jour, ainsi
 * que leur (nouvelle) valeur.
 */
function majEtatEtPage(etatCourant, champsMisAJour) {
  const nouvelEtat = { ...etatCourant, ...champsMisAJour };
  majPage(nouvelEtat);
}

/**
 * Prend une structure décrivant les callbacks à enregistrer et effectue les
 * affectation sur les bon champs "on...". Par exemple si callbacks contient la
 * structure suivante où f1, f2 et f3 sont des callbacks:
 *
 * { "btn-pokedex": { "onclick": f1 },
 *   "input-search": { "onchange": f2,
 *                     "oninput": f3 }
 * }
 *
 * alors cette fonction rangera f1 dans le champ "onclick" de l'élément dont
 * l'id est "btn-pokedex", rangera f2 dans le champ "onchange" de l'élément dont
 * l'id est "input-search" et rangera f3 dans le champ "oninput" de ce même
 * élément. Cela aura, entre autres, pour effet de délclencher un appel à f1
 * lorsque l'on cliquera sur le bouton "btn-pokedex".
 *
 * @param {Object} callbacks dictionnaire associant les id d'éléments à un
 * dictionnaire qui associe des champs "on..." aux callbacks désirés.
 */
function enregistreCallbacks(callbacks) {
  Object.keys(callbacks).forEach((id) => {
    const elt = document.getElementById(id);
    if (elt === undefined || elt === null) {
      console.log(
        `Élément inconnu: ${id}, 
        impossible d'enregistrer de callback sur cet id`
      );
    } else {
      Object.keys(callbacks[id]).forEach((onAction) => {
        elt[onAction] = callbacks[id][onAction];
      });
    }
  });
}

/**
 * Mets à jour la page (contenu et événements) en fonction d'un nouvel état.
 *
 * @param {Etat} etatCourant l'état courant
 */
function majPage(etatCourant) {
  console.log("CALL majPage",etatCourant);
  const page = generePage(etatCourant);
  document.getElementById("root").innerHTML = page.html;
  enregistreCallbacks(page.callbacks);
}

/**
 * Appelé après le chargement de la page.
 * Met en place la mécanique de gestion des événements
 * en lançant la mise à jour de la page à partir d'un état initial.
 */
function initClientPokemons() {
  console.log("CALL initClientPokemons");
  const etatInitial = {
    loginModal: false,
    login: undefined,
    errLogin: undefined
    
  };
  recuperePokemon(etatInitial);
}

/**
 * Génère l'html de notre bouton rechercher
 * @param {Etat} etatCourant 
 * @returns html
 */
function genereSearchButton(){
  return `
    <div class="navbar-item">
      <div class="buttons">
        <p id="search-barre">
          <input type="search" id="pokemon-name" 
          placeholder="Info textarea" value=''> 
        </p>
      </div>
    </div>`;
}

/**
 * Generer de l'html qui créer un boutton un callbacks indiquant
 * que tous les pokemons sont de nouveau affichés
 * @param {Etat} etatCourant 
 * @returns un champ html et un callbacks 
 */
function genereRefreshPokemon(etatCourant){
  const html=
  `<div class="navbar-item">
    <div class="buttons">
      <button id="btn-refresh" class="button is-light">
        afficher tout les pokemon
      </a>
    </div>
  </div>`
  return {
    html:html,
    callbacks:{
      "btn-refresh":{
        onclick: ()=>majEtatEtPage(etatCourant, {search:''}),
      },
    }
  }
}


/**
 * Génère un bouton rechercher en HTML qui lorsqu'on clique déclanche les pokemon associés
 * à la chaine de carctère inscrite
 * @param {Etat} etatCourant
 * @return un champ html qui fait le bouton recherche
 */
 function genereSearchPokemon(etatCourant) {
  const html=genereSearchButton();
  return {
    html: html,
    callbacks: {
      "pokemon-name": {
        onchange: () =>{
          document.getElementById("pokemon-name").value !=='' ? 
          searchPokemon(etatCourant,
            document.getElementById("pokemon-name").value):
          searchPokemon(etatCourant,'');
        },
      },
    },
  };
}

/**
 * Une fonction qui récupère les pokemons et filtre le tableau
 * pour ne garder que les pokemons associés à une chaine de caratère
 * @param {Etat} etatCourant
 * @returns une promesse
 */
function searchPokemon(etatCourant, pokemonName=''){
  majEtatEtPage(etatCourant,{search: pokemonName});
}

/**
 * Filtre dans les noms dans le tableau de pokemon pour
 * retourner une promesse que la chaîne de caractère présente dans
 * le champ search soit présente dans les noms qui composent le tableau
 * de nom de pokemon.
 * @param {Etat} etatCourant 
 * @returns Une promesse
 */
function filterPokemon(etatCourant){
  return etatCourant.pokemons
  .filter((pokemon)=>pokemon.Name.toLowerCase()
  .search(etatCourant.search.toLowerCase())>=0);
}